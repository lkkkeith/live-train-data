import React from "react";
import { LineCodeEnum, DirectionEnum } from '../globals'
import { TrainHead } from '../components/TrainHead'
import { TrainCar } from '../components/TrainCar'

export default function Train({data}) {

    return(
        <>
            {
                data.length > 0 ?
                data.map( (train,index) => {
                    let color = LineCodeEnum[train.LineCode] ? LineCodeEnum[train.LineCode].toLowerCase() : '';
                    let trainCars = [];

                    for (let i = 0; i < train.CarCount; i++) {
                        trainCars.push(<TrainCar key={i} lineColor={color} carNumber={i+1} />);
                    }
                    return(                        
                        <div key={index}>
                            <p className="direction">{DirectionEnum[train.DirectionNum]}</p>
                            <TrainHead serviceType={train.ServiceType} direction="left" lineColor={color} trainNumber={train.TrainNumber} />
                            {trainCars}
                            <TrainHead serviceType={train.ServiceType} direction="right" lineColor={color} trainNumber={train.TrainNumber} />
                        </div>
                        
                    )
                })
                :
                <span>No result found</span>
            }
        </>
    )
};
