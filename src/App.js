import './App.css';
import React, { useEffect, useState } from 'react';
import { LineCodeEnum, ServiceTypeEnum } from './globals'
import Train from './components/Train';

function App() {

  const [isFetching, setIsFetching] = useState(false);
  const [trainPositions, setTrainPositions] = useState();
  const [carCountFilter, setCarCountFilter] = useState('');
  const [trainLineFilter, setTrainLineFilter] = useState('');
  const [serviceTypeFilter, setServiceTypeFilter] = useState('');

  const sortOrder = ['Normal', 'NoPassengers', 'Special', 'Unknown'];

  const END_POINT = "https://api.wmata.com/TrainPositions/TrainPositions?contentType={json}"
  const API_KEY = "7466305b37a34afcbd6a58f26194123e"
  const REFRESH_TIME = 10;
  let url = new URL(END_POINT)

  const getTrainData = async () => {
    console.log( new Date().toISOString() );

    setIsFetching(true)
    const res = await fetch(url, { headers: { 'api_key': API_KEY } });
    let jsonResponse = await res.json();
    let trainData = jsonResponse.TrainPositions;

    // update line code value to a string 'null' if it's null
    trainData = trainData.map( (item) => !item.LineCode ? {...item, LineCode: "null"} : item ); 

    // Sort by service type
    trainData.sort((a, b) => sortOrder.indexOf(a.ServiceType)- sortOrder.indexOf(b.ServiceType));

    setTrainPositions(trainData);
    setIsFetching(false)
  }
  
  // Fetch for "component mount"
  useEffect(() => {
    getTrainData();
  }, []);

  // Fetch data every REFRESH_TIME seconds
  useEffect(() => {
    const timer = setInterval(getTrainData, 1000 * REFRESH_TIME);

    // clean timer upon dismount
    return () => { clearInterval(timer) }
  }, []);


  const handleCarCountChange = (e) => {
    let amount = parseInt(e.target.value);
    setCarCountFilter( (isNaN(amount) || amount < 0) ? '' : amount )
  }

  const handleTrainLineChange = (val) => {
    setTrainLineFilter(val)
  }

  const handleServiceTypeChange = (val) => {
    setServiceTypeFilter(val)
  }

  const handleClearFilters = (e) => {
    e.preventDefault();
    setTrainLineFilter('')
    setServiceTypeFilter('')
    setCarCountFilter('')
  }

  function search(rows) {
    let result = rows;

    if (trainLineFilter !== ''){
      result = result.filter(row => row.LineCode === trainLineFilter )
    }

    if (serviceTypeFilter !== ''){
      result = result.filter(row => row.ServiceType === serviceTypeFilter )
    }

    if (carCountFilter !== '' && carCountFilter >= 0){
      result = result.filter(row => row.CarCount === carCountFilter )
    }
    
    return result
  }

  return (
    <div className="row">

      <h1>Live Train Data</h1>

      <div className='col-12'>
        <form className='form-inline'>
          <fieldset disabled={isFetching}>
            <legend>Filters</legend>

            <label htmlFor="trainline">Train Line Color :</label>
            <select name="trainline" value={trainLineFilter} onChange={ e => handleTrainLineChange(e.target.value) }>
              <option value="">Any</option>
              { Object.keys(LineCodeEnum).map((o_key, i) => <option key={i} value={o_key}>{LineCodeEnum[o_key]}</option> ) }
            </select>

            <label htmlFor="service_type">Service Type :</label>
            <select name="service_type" value={serviceTypeFilter} onChange={e => handleServiceTypeChange(e.target.value) }>
              <option value="">Any</option>
              { Object.keys(ServiceTypeEnum).map((o_key, i) => <option key={i} value={o_key}>{ServiceTypeEnum[o_key]}</option>) }
            </select>
          
            <label htmlFor="carCount">Car Count :</label>
            <input name="carCount" type="number" min={0} max={999} value={carCountFilter} className="car-count" onChange={handleCarCountChange}></input>

            <button onClick={e => handleClearFilters(e) }>Show All Trains</button>
          </fieldset>
        </form>
          
      </div>

      <div className='col-12'>
        <div className="container">
        {trainPositions && search(trainPositions).length > 0 && <div>Showing {search(trainPositions).length} Result(s)</div>}
        {trainPositions && <Train data={search(trainPositions)} />}
        </div>
      </div>
    </div>
  );
}

export default App;
