const LineCodeEnum = {
    "RD": "Red",
    "BL": "Blue",
    "YL": "Yellow",
    "OR": "Orange",
    "GR": "Green",
    "SV": "Silver",
    "null": "Null",
}

const ServiceTypeEnum = {
    "NoPassengers": "No Passengers",
    "Normal"      : "Normal",
    "Special"     : "Special",
    "Unknown"     : "Unknown",
}

const DirectionEnum = {
    1: "Northbound/ Eastbound",
    2: "Southbound/ Westbound"
}

export { LineCodeEnum, ServiceTypeEnum, DirectionEnum };
